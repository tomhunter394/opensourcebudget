<%-- 
    Document   : Home
    Created on : Mar 22, 2016, 7:16:46 AM
    Author     : Ben
--%>
<%@include file="/WEB-INF/jspf/template_defs.jsp" %>
<%@include file="/WEB-INF/jspf/template_head.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <!-- Google charts, pi chart -->
         <div id="piechart" style="width: 900px; height: 500px;"></div>
    </body>
</html>

<%@include file="/WEB-INF/jspf/template_tail.jsp" %>
